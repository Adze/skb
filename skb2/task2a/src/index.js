import express from 'express';

const app = express();

app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.get('/', (req, res) => {
  let a = 0;
  let b = 0;
  a = parseInt(req.query.a, 10);
  if (isNaN(a)) {
    a = 0;
  }
  b = parseInt(req.query.b, 10);
  if (isNaN(b)) {
    b = 0;
  }
  res.send((a + b).toString());
});

app.listen(3000, () => {
  console.log('Example app listening on port 3000!');
});
