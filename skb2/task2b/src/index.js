import express from 'express';

const app = express();

app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.get('/', (req, res) => {
  if (req.query.fullname.match(/([\d|_]+|^$|\/)/)) {
    return res.send('Invalid fullname');
  }
  const fullnameArr = req.query.fullname.toLowerCase().match(/([^\d\s]+)/g);
  switch (fullnameArr.length) {
    case 3:
      res.send(fullnameArr[2][0].toUpperCase() + fullnameArr[2].substr(1) + ' ' + fullnameArr[0][0].toUpperCase() + '. ' + fullnameArr[1][0].toUpperCase() + '.');
      break;
    case 2:
      res.send(fullnameArr[1][0].toUpperCase() + fullnameArr[1].substr(1) + ' ' + fullnameArr[0][0].toUpperCase() + '.');
      break;
    case 1:
      res.send(fullnameArr[0][0].toUpperCase() + fullnameArr[0].substr(1));      
      break;
    default:
      res.send('Invalid fullname');
      break;
  }
});

app.listen(3000, () => {
  console.log('Example app listening on port 3000!');
});
