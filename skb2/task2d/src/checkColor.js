import hsl from 'hsl-to-hex';

function rgbToHex(rgbArray) {
    let result = '#';

    for (let index = 2; index < 5; index++) {
        if(rgbArray[index] < 0 || rgbArray[index] > 255) {
            result = 'Invalid color';
            break;
        }
        if(rgbArray[index] > 15) {
            result += Number(rgbArray[index]).toString(16);
        } else {
            result +=  '0' + Number(rgbArray[index]).toString(16);
        }
    }
    
    return result;
}

function hslToHex(hslArray) {
    if(hslArray[2] < 0 || hslArray[2] > 360) {
        return 'Invalid color';
    }

    const s = parseInt(hslArray[3]);
    const l = parseInt(hslArray[4]);

    if(s < 0 || s > 100) {
        return 'Invalid color';
    }
    if(l < 0 ||l > 100) {
        return 'Invalid color';
    }
    return hsl(hslArray[2], s, l);
}

export default function checkColor(color) {
    if(color === undefined) {
        return 'Invalid color';
    }
    const hex = new RegExp(/^\s*#?((?:[\da-f]{3}){1,2})\s*$/i);
    const rgb = new RegExp(/^\s*(rgb)\(\s*(\d{1,3})\s*,\s*(\d{1,3})\s*,\s*(\d{1,3})\s*\)\s*$/i);
    const hsl = new RegExp(/^\s*(hsl)\(\s*(\d{1,3})\s*,\s*(\d{1,3}%)\s*,\s*(\d{1,3}%)\s*\)\s*$/i);
    
    let clearColor = color.replace(/%20/g, ' ');
    let result = clearColor.match(hex);
    
    if(result === null) {
        result = clearColor.match(rgb);
        if(result === null) {
            result = clearColor.match(hsl);
            if(result === null) {
                return 'Invalid color';
            }
            if(('hsl').localeCompare((result[1].toLowerCase())) == 0) {
                return hslToHex(result);
            }
        }
        if(('rgb').localeCompare((result[1].toLowerCase())) == 0) {
            return rgbToHex(result);
        }
    } 
    if(result[1].length == 3) {
        return ('#' 
            + result[1][0] + result[1][0] 
            + result[1][1] + result[1][1] 
            + result[1][2] + result[1][2])
            .toLowerCase();
    } else {
        return ('#' + result[1]).toLowerCase();
    }
}
