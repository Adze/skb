import express from 'express';
import cors from 'cors';
import _ from 'lodash';
import checkColor from './checkColor'

const app = express();
let testNum = 1;

//CORS
app.use(cors());

//Route for /
app.get('/', (req, res) => {
  console.log('Test ' + testNum++ + ': ' + req.query.color);
  const color = checkColor(req.query.color);
  console.log(color);
  res.send(color);
});

app.listen(3000, () => {
  console.log('Server listening on port 3000...');
});
