import express from 'express';
import cors from 'cors';
import Scrambo from 'scrambo';

const app = express();

app.use(cors());
// const arr = [1, 18, 243, 3240, 43254];
const arr = [
  '1',
  '18',
  '243',
  '3240',
  '43254',
  '577368',
  '7706988',
  '102876480',
  '1373243544',
  '18330699168',
  '244686773808',
  '3266193870720',
  '43598688377184',
  '581975750199168',
  '7768485393179328',
  '103697388221736960',
  '1384201395738071424',
  '18476969736848122368',
  '246639261965462754048',
  '3292256598848819251200',
  '43946585901564160587264'];

const test = new Scrambo();

app.get('/', (req, res) => {
  console.log('Query = ' + req.query.i + '; Result = ' + arr[req.query.i]);

  return res.send(arr[req.query.i]);
});

app.listen(3000, () => {
  console.log('Example app listening on port 3000!');
});
