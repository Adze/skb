export default function canonize(url) {
  //const re = new RegExp(/@?(.*\/)?([\w-.]*)/i);
  const re = new RegExp(/@?(((https?:)?\/\/)?.*?\/)?([@\w-.]+)+([\w-.]*)?/i);
  const username = url.match(re);
  if (username[4][0] != '@') {
    return '@'+username[4];
  } else {
    return username[4];
  }
}