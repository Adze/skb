// import express from 'express';
// import cors from 'cors';
// import fetch from 'isomorphic-fetch';
// import _ from 'lodash';
import mongoose from 'mongoose';
import Promise from 'bluebird';

//const app = express();

//CORS
//app.use(cors());

mongoose.Promise = Promise;
mongoose.connect('mongodb://localhost:27017/skb');
const Pet = mongoose.model('Pet', {
  type: String,
  name: String,
});

const kitty = new Pet({
  name: 'Zildjian',
  type: 'cat',
});

kitty.save()
  .then(() => {
    console.log('success');
  })
  .catch((err) => {
    console.log('err', err);
  });

// app.get('/', (req, res) => {
//   try {
//     res.status(200).json(pc);
//   } catch(err) {
//     console.log(err);
//   }
// });

// app.listen(3000, () => {
//   console.log('Server listening on port 3000...');
// });
