import express from 'express';
import cors from 'cors';
import fetch from 'isomorphic-fetch';
import _ from 'lodash';

const app = express();

//CORS
app.use(cors());

const pcUrl = 'https://gist.githubusercontent.com/isuvorov/ce6b8d87983611482aac89f6d7bc0037/raw/pc.json';

//Get pc.json
let pc = {};
fetch(pcUrl)
  .then(async (res) => {
    pc = await res.json();
  })
  .catch(err => {
    console.log('Error:', err);
  });
  
//Calculate volumes size
function getVolumesSize(arrVolumes) {
  let result = {};
  let volume = null;

  arrVolumes.forEach((drive) => {
    volume = drive.volume;
    if(!result[volume]) {
      result[volume] = drive.size;
    } else {
      result[volume] += drive.size;
    }
  });

  for( let key in result)
  {
    result[key] += 'B';
  }

  return result;
}

//Check path for own property
function checkParams(path) {
  let result = true;
  
  const arrPath = _.split(path ,'.');
  if((arrPath.length >= 2) && (isNaN(arrPath[arrPath.length - 1])))
  {
    if(_.has(arrPath[arrPath.length - 2], arrPath[arrPath.length - 1])) {
      result = false;
    }
  }

  return result;
}

//Route for Volumes
app.get('/volumes', (req, res) => {
  try {
    const result = _.get(pc, 'hdd');
    res.status(200).json(getVolumesSize(result));
  } catch(err) {
    console.log(err);
  }
});

//Route for /
app.get('/', (req, res) => {
  try {
    res.status(200).json(pc);
  } catch(err) {
    console.log(err);
  }
});

//Default route
app.use((req, res) => {
  try {
    //Check "." in query
    if(req.path.indexOf('.') != -1) {
      return res.status(404).send('Not Found');
    }
    //Replace "/" to "." and trim "." at start and end of string
    const path = _.trim(req.path.replace(/\//g, "."), '.');
    //Check path
    if(!checkParams(path)) {
      return res.status(404).send('Not Found');
    }
    const result = _.get(pc, path);
    if(result === undefined){
      return res.status(404).send('Not Found');
    }
    res.status(200).json(result);
  } catch(err) {
    console.log(err);
    return res.json({ err });
  }
});

app.listen(3000, () => {
  console.log('Server listening on port 3000...');
});
