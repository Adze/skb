import express from 'express';
import fetch from 'isomorphic-fetch';
import Promise from 'bluebird';
import _ from 'lodash';

const __DEV__ = true;

const app = express();

const baseUrl = 'https://pokeapi.co/api/v2';
const pokemonFields =  ['id', 'name', 'bulbasaur', 'base_experience', 'height', 'is_default', 'order', 'weight'];

async function getPokemons(url, i = 0) {
  //console.log('getPokemons ', url, i);
  const response = await fetch(url);  
  const page = await response.json();
  const pokemons = page.results;
  if (__DEV__ && i >= 1) {
    return pokemons;
  }

  if (page.next) {
    const pokemons2 = await getPokemons(page.next, ++i);
    return [
      ...pokemons,
      ...pokemons2
    ];
  }

  return pokemons;
};

async function getPokemonWeight(id) {
  const response = await fetch(`${baseUrl}/pokemon/${id}`);  
  const pokemon = await response.json();
  return pokemon;
};

async function getPokemon(url) {
  const response = await fetch(url);
  const pokemon = await response.json();
  return pokemon;
}

const pokemonsUrl = `${baseUrl}/pokemon`;
getPokemons(pokemonsUrl).then(pokemons => {
  //console.log(pokemons.length);
});

app.get('/', async (req, res) => {
  try {
    const pokemonsUrl = `${baseUrl}/pokemon`;
    const pokemonsInfo = await getPokemons(pokemonsUrl);
    const pokemonsPromises = pokemonsInfo.map(info => {
      return getPokemon(info.url);
    });

    const pokemonsFull = await Promise.all(pokemonsPromises);
    const pokemons = pokemonsFull.map((pokemon) => {
      return _.pick(pokemon, pokemonFields);
    });

    const sortPokemons = _.sortBy(pokemons, pokemon => -pokemon.weight);

    return res.json(sortPokemons);
  } catch(err) {
    console.log(err);
    return res.json({ err });
  }
});


app.listen(3000, () => {
  console.log('Server listening on port 3000...');
});
